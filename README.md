# atom-comment-snippets package

A port of the famous [Sublime Snippets](https://github.com/hachesilva/Comment-Snippets) package for Atom

## Comments Examples

### PHP Comments

#### PHP Section

Tab trigger: `comm-section-PHP` + `tab`

```html
<!--=============================================>>>>>
=              Section comment block                 =
===============================================>>>>-->



<!--=          End of Section comment block       =-->
<!--==========================================<<<<<-->
```

#### PHP Section header

Tab trigger: `comm--header-PHP` + `tab`

```html
<!--=============================================>>>>>
=              Section comment block                 =
===============================================>>>>-->
```

#### PHP Section footer

Tab trigger: `comm-footer-PHP` + `tab`

```html
<!--=        End of Section comment block      =   -->
<!--==========================================<<<<<-->
```

#### PHP Comment

Tab trigger: `comm-PHP` + `tab`

```html
<!-- html comment -->
```

### C-Style Comments

For languages supporting C-Style comments: PHP, CSS, Javascript, Java...

#### Section

Tab trigger: `comm-section` + `tab`


```js
/*=============================================>>>>>
=            Section comment block                =
===============================================>>>>>*/



/*=         End of Section comment block =          */
/*=============================================<<<<<*/
```

#### Section Header

Tab trigger: `comm-section-header` + `tab`

```js
/*===============================================>>>>>
=             Section comment block                  =
===============================================>>>>>*/
```

#### Section Footer

Tab trigger: `comm-section-footer` + `tab`

```css
/*=           End of Section comment block         =*/
/*=============================================<<<<<*/
```

#### Subsection

Tab trigger: `comm-subsection` + `tab`

```css
/*----------- Subsection comment block -----------*/
```


#### Simple Comment

Tab trigger: `comm` + `tab`

```css
/* Comment */
```

#### Block Comment

Tab trigger: `comm-block` + `tab`

```css
/**
 *
 * Block comment
 *
 */
```